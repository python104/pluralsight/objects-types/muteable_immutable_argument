import time
print(time.ctime())


def show_default(arg=time.ctime()):
    print(arg)
##############################
##############################
show_default()
def add_spam(menu=[]):   # muteable argument (values can be change when called)
     menu.append("spam")
     return menu

breakfast = ["egg, tea"]
add_spam(breakfast) #this will add one spam in breakfast
print(breakfast)
add_spam(breakfast) # this will add another spam in breakfast
print(breakfast)
print(add_spam())  # this will add one spam in empty list
print(add_spam())  # this will add one more spam in list containing spam 
print(add_spam())  # this will add one more spam in list containing spam spam

#how to avoid this
#use immuteable object such as  strings or integer for default value
#here we use None object as sentinal.
def add_spam2(menu=None):
    if menu is None:
        menu=[]
    menu.append("spam")
    return menu

print('this wont add spam repeatedly')
print(add_spam2()) 
print(add_spam2()) 
print(add_spam2()) 